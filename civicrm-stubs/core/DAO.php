<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CRM_Core_DAO 
{
    /**
     *
     * @var /PDOStatement
     */
    public $statement;
    /**
     *
     * @var \PDO
     */
    private $pdo;
    
    public function __construct() {
        $this->Pdo();
    }

    private function Pdo()
    {
        if ($this->pdo == null){
            $this->pdo = new \PDO('../../data/db.sqlite');
        }
    }

    /**
     * 
     * @param string $sql
     * @return /PDOStatement | null
     */
    public static function executeQuery($sql)
    {
        $dao = new CRM_Core_DAO();
        $dao->statement = $dao->pdo->prepare($sql);
        return ($dao->statement->execute()) ?
        $dao->statement : null;
    }
}